# Colored

**Cangjie Version: `>= 0.53.4`**

![Screenshot](./_assets/img1.png)

> 🎨 Adding color to your terminal has never been easier.

## Features

- Support foreground colors, background colors and styles
- Support for 256 colors and 24-bit colors

## Install

Add following to your `cjpm.toml` file:

```toml
[dependencies]
colored = { git = "https://gitcode.com/elon_tang/colored.git", tag = "0.0.1" }
```

## Usage

```cangjie
import colored.{Color, Colorizable, Colored}

main(): Unit {
    println("red".red())
    println("bold italic".bold().italic())
    println("trueColor".color(Color.TrueColor(53, 49, 117)))
}
```

## License

Mulan PSL v2.

## Author

[Elon Tang](https://github.com/blackcater)
